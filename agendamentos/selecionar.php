<?php
//função para trazer agendamentos para tabela
function sistema_agendamentos(){
  include "conexao_db/conexao.php";
  $sql = "SELECT a.id,a.dt_agendamento,a.dt_agendado,a.status, e.nome as nomeresp, cc.nome as nomecliente, cc.telefone as tellcliente, e.telefone as tellequipe FROM agendamentos a , equipe e , chamados c, clientes cc WHERE a.chamado=c.id and a.equipe=e.id and cc.id=c.cliente";
  $result=mysqli_query($link, $sql);
  
  if (!$result):
    echo "erro na consulta do banco";
  else:
    while($linha=mysqli_fetch_array($result)){
      $id=$linha['id'];
      if($linha['status']== 0):
        $i = "<i class='material-icons'>check</i>";
      else:
        $i = "<i class='material-icons'>clear</i>";
      endif;
      ?>
      <tr>
            <td><?=$linha['dt_agendamento']?></td>
            <td><?=$linha['nomecliente']?></td>
            <td><?=$linha['tellcliente']?></td>
            <td><?=$linha['nomeresp']?></td>
            <td><?=$linha['tellequipe']?></td>
            <td><?=$linha['dt_agendado']?></td>
            <td><?=$i?></td>
            <td>
              <button class="btn-floating btn-medium waves-effect waves-light red" alt="Excluir" onclick="funcaoApagarAgendamento(<?=$linha['id']?>)"><i class="material-icons">delete</i></button>
              <?php
              if($linha['status']!= 0):
                ?>
                <button class="btn-floating btn-medium waves-effect waves-light green" alt="Excluir" onclick="funcaoCompletarAgendamento(<?=$id?>)"><i class="material-icons">done_all</i></button>
              <?php
              endif;
              ?>
              
              
            </td>
      </tr>
      <?php
    }
  endif;
}