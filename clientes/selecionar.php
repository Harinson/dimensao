<?php
//verificação para chamar a função pelo ajax com if
if(isset($_POST['funcao']) && $_POST['funcao']==2):
  edicao_clientes($_POST['id']);
endif;

// funcao para pegar os clientes e mostrar na tabela
function sistema_clientes(){
  include "conexao_db/conexao.php";
  $sql = "SELECT * FROM clientes";
  $result=mysqli_query($link, $sql);
  if (!$result):
    echo "erro na consulta do banco";
  else:
    while($linha=mysqli_fetch_array($result)){
      $id=$linha['id'];
      ?>
      <tr>
            <td><?=$linha['nome']?></td>
            <td><?=$linha['endereco']?></td>
            <td><?=$linha['telefone']?></td>
            <td><?=$linha['email']?></td>
            <td>
              <button class="btn-floating btn-medium waves-effect waves-light blue" alt="Editar" onclick="funcaoEditarCliente(<?=$linha['id']?>)"><i class="material-icons">edit</i></button>
              <button class="btn-floating btn-medium waves-effect waves-light red" alt="Excluir" onclick="funcaoApagarCliente(<?=$linha['id']?>)"><i class="material-icons">delete</i></button>
            </td>
      </tr>
      <?php
    }
  endif;
}
// funcao para pegar os dados dos clientes e mostrar no modal de edicao
function edicao_clientes($id){
  include "../conexao_db/conexao.php";
  $sql = "SELECT * FROM clientes WHERE id='{$id}'";
  $result=mysqli_query($link, $sql);
  if (!$result):
    echo "erro na consulta do banco";
  else:
    while($linha=mysqli_fetch_array($result)){
      ?>
      <form class="col s12" method="post" action="clientes/atualizar.php">
      <div class="row">
        <!-- input invisivel com o valor do ID -->
        <input name="id"  id="id" type="text" class="validate" value="<?=$linha['id']?>" style="display:none;">
          
        
        <div class="input-field col s12">
          <i class="material-icons prefix">account_circle</i>
          <input  name="nome"  id="nome" type="text" class="validate" value="<?=$linha['nome']?>">
          <label class="active" for="nome">Nome Completo</label>
        </div>
        <div class="input-field col s12">
          <i class="material-icons prefix">location_on</i>
          <input name="endereco" id="endereco" type="text" class="validate" data-lenght="100" value="<?=$linha['endereco']?>">
          <label class="active" for="endereco">Endereco</label>
        </div>
        <div class="input-field col s6">
          <i class="material-icons prefix">phone</i>
          <input name="tell"  id="tell" type="tel" class="validate" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" value="<?=$linha['telefone']?>">
          <label class="active" for="tell">Telefone</label>
        </div>
        <div class="input-field col s6">
          <i class="material-icons prefix">email</i>
          <input name="mail"  id="mail" type="email" class="validate" value="<?=$linha['email']?>">
          <label class="active" for="mail">E-mail</label>
        </div>
        <button style="float: right;" name="btn_att" class="modal-close btn waves-effect waves-light blue" type="submit" name="action">Atualizar
          <i class="material-icons right">edit</i>
         </button>
      </div>
    </form>
      <?php
    }
  endif;
}
//gerenciamento de clientes que efetuam a ligação para empresa
function clientes_chamados(){
  include "conexao_db/conexao.php";
  $sql = "SELECT * FROM clientes";
  $result=mysqli_query($link, $sql);
  if (!$result):
    echo "erro na consulta do banco";
  else:
      while($linha=mysqli_fetch_array($result)){
        ?>
          <option value="<?=$linha['id']?>"><?=$linha['nome']?></option>
        <?php
      }
     
  endif;
}