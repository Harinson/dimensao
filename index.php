<?php
  include_once "header.php";
?>
  <!-- Menu Principal -->
  <nav class="blue lighten-2">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo">Sistema Dimensão</a>
    </div>
  </nav>
  <!-- Escolha do sistema -->
  <div class="row">
    <ul class="tabs">
      <li class="tab col s3"><a  class="active"  href="#chamados">Chamados</a></li>
      <li class="tab col s3"><a href="#clientes">Clientes</a></li>
      <li class="tab col s3"><a  href="#equipe">Equipe</a></li>
      <li class="tab col s3"><a href="#agendamentos">Agendamentos</a></li>
    </ul>
  </div>
  <!-- containers do sistema -->
  <section class="container">

    <!-- Tabela de Chamadas -->
    <div id="chamados" class="col s12">
      <div class="titulo-chamados">
        <h3>Chamados <a class="btn-floating btn-large waves-effect waves-light green modal-trigger" href="#addChamado"><i
              class="material-icons">add</i></a></h3>
      </div>

      <table class="responsive-table">
        <thead>
          <tr>
            <th>Data de Contato</th>
            <th>Cliente</th>
            <th>Motivo</th>
            <th>Descrição</th>
            <th>Status</th>
            <th>Ação</th>
          </tr>
        </thead>
        <tbody>
            <!-- Cjamado para a pagina e função que mostra os itens da tabela Clientes -->
            <?php
            include_once "chamados/selecionar.php";
            sistema_chamados();
            ?>

        </tbody>
      </table>
    </div>

    <!-- Tabela Clientes -->
    <div id="clientes" class="col s12">
      <!-- Titulo e botão adicionar -->
      <div class="titulo-clientes">
        <h3>Clientes <a class="btn-floating btn-large waves-effect waves-light green modal-trigger" href="#addCliente"><i
              class="material-icons">add</i></a></h3>
      </div>

      <table class="responsive-table">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Endereço</th>
            <th>Telefone</th>
            <th>E-mail</th>
            <th>Ação</th>
          </tr>
        </thead>
        <tbody>
            <!-- Cjamado para a pagina e função que mostra os itens da tabela Clientes -->
            <?php
            include_once "clientes/selecionar.php";
            sistema_clientes();
            ?>

        </tbody>
      </table>
    </div>

    <!-- Tabela da Equipe-->
    <div id="equipe" class="col s12">
      <!-- Titulo e botão adicionar -->
      <div class="titulo-equipe">
        <h3>Equipe <a class="btn-floating btn-large waves-effect waves-light green modal-trigger" href="#addEquipe"><i
              class="material-icons">add</i></a></h3>
      </div>

      <table class="responsive-table">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Função</th>
            <th>Telefone</th>
            <th>Ação</th>
          </tr>
        </thead>

        <tbody>
          
            <?php
            include_once "equipe/selecionar.php";
            sistema_equipe();
            ?>
          

        </tbody>
      </table>
    </div>
    <!-- Tabela de Agendamentos -->
    <div id="agendamentos" class="col s12">
      <div class="titulo-equipe">
        <h3>Agendamentos <a class="btn-floating btn-large waves-effect waves-light green modal-trigger" href="#addAgendamento"><i
              class="material-icons">add</i></a></h3>
      </div>

      <table class="responsive-table">
        <thead>
          <tr>
            <th>Data de Agendamento</th>
            <th>Cliente</th>
            <th>Contato do Cliente</th>
            <th>Responsável</th>
            <th>Contato do Responsável</th>
            <th>Data Agendada</th>
            <th>Status</th>
            <th>Ação</th>
          </tr>
        </thead>

        <tbody>
          
            <?php
            include_once "agendamentos/selecionar.php";
            sistema_agendamentos();
            ?>
          

        </tbody>
      </table>
    </div>
  </section>

  <?php
    include_once "modals.php";
  ?>
  <?php
  include_once "footer.php";
?>
