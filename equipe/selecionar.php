<?php
//verificação para chamar a função pelo ajax com if
if(isset($_POST['funcao']) && $_POST['funcao']==2):
  edicao_equipe($_POST['id']);
endif;

// funcao para pegar a equipe e mostrar na tabela
function sistema_equipe(){
  include "conexao_db/conexao.php";
  $sql = "SELECT * FROM equipe";
  $result=mysqli_query($link, $sql);
  if (!$result):
    echo "erro na consulta do banco";
  else:
    while($linha=mysqli_fetch_array($result)){
      $id=$linha['id'];
      ?>
      <tr>
            <td><?=$linha['nome']?></td>
            <td><?=$linha['cargo']?></td>
            <td><?=$linha['telefone']?></td>
            <td>
              <button class="btn-floating btn-medium waves-effect waves-light blue" alt="Editar" onclick="funcaoEditarEquipe(<?=$linha['id']?>)"><i class="material-icons">edit</i></button>
              <button class="btn-floating btn-medium waves-effect waves-light red" alt="Excluir" onclick="funcaoApagarEquipe(<?=$linha['id']?>)"><i class="material-icons">delete</i></button>
            </td>
      </tr>
      <?php
    }
  endif;
}

// funcao para pegar os dados da equipe e mostrar no modal de edicao
function edicao_equipe($id){
  include "../conexao_db/conexao.php";
  $sql = "SELECT * FROM equipe WHERE id='{$id}'";
  $result=mysqli_query($link, $sql);
  if (!$result):
    echo "erro na consulta do banco";
  else:
    while($linha=mysqli_fetch_array($result)){
      ?>
      <form class="col s12" method="post" action="equipe/atualizar.php">
      <div class="row">
        <!-- input invisivel com o valor do ID -->
        <input name="id"  id="id" type="text" class="validate" value="<?=$linha['id']?>" style="display:none;">
          
        
        <div class="input-field col s12">
          <i class="material-icons prefix">account_circle</i>
          <input  name="nome_equipe"  id="nome_equipe" type="text" class="validate" value="<?=$linha['nome']?>">
          <label class="active" for="nome_equipe">Nome Completo</label>
        </div>
        <div class="input-field col s6">
          <i class="material-icons prefix">build</i>
          <input name="funcao_equipe"  id="funcao_equipe" type="text" class="validate" value="<?=$linha['cargo']?>">
          <label class="active" for="funcao_equipe">Função</label>
        </div>
        <div class="input-field col s6">
          <i class="material-icons prefix">phone</i>
          <input name="tell_equipe"  id="tell_equipe" type="tel" class="validate" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" value="<?=$linha['telefone']?>">
          <label class="active" for="tell_equipe">Telefone</label>
        </div>
        <button style="float: right;" name="btn_att" class="modal-close btn waves-effect waves-light blue" type="submit" name="action">Atualizar
          <i class="material-icons right">edit</i>
         </button>
      </div>
    </form>
      <?php
    }
  endif;
}
//gerenciamento da equipe que dara suporte ao chamado do cliente
function equipe_agd(){
  include "conexao_db/conexao.php";
  $sql = "SELECT * FROM equipe";
  $result=mysqli_query($link, $sql);
  if (!$result):
    echo "erro na consulta do banco";
  else:
      while($linha=mysqli_fetch_array($result)){
        ?>
          <option value="<?=$linha['id']?>"><?=$linha['nome']?></option>
        <?php
      }
     
  endif;
}