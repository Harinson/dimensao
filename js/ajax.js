// função para fazer a exclusão pratica de clientes
function funcaoApagarCliente(val) {
  var x = val;
  var r = confirm("Tem certeza que deseja excluir?");
  if (r == true) {
    $.ajax({
      url: "clientes/excluir.php",
      type: "POST",
      data: { 'id': x },
      success: function (res) {
        alert(res);
        location.reload();
      }
    });
  }
}
//função para editar o cliente
function funcaoEditarCliente(val) {
  var instance = M.Modal.getInstance(document.getElementById('editCliente'));
  var x = val;
  $.ajax({
    url: "clientes/selecionar.php",
    type: "POST",
    data: { 'id': x, 'funcao': 2 },
    success: function (res) {
      document.getElementById('editar_cliente').innerHTML = res;
      instance.open();
    }
  });
}
// função para fazer a exclusão pratica da equipe
function funcaoApagarEquipe(val) {
  var x = val;
  var r = confirm("Tem certeza que deseja excluir?");
  if (r == true) {
    $.ajax({
      url: "equipe/excluir.php",
      type: "POST",
      data: { 'id': x },
      success: function (res) {
        alert(res);
        location.reload();
      }
    });
  }
}
//função para editar a equipe
function funcaoEditarEquipe(val) {
  var instance = M.Modal.getInstance(document.getElementById('editEquipe'));
  var x = val;
  $.ajax({
    url: "equipe/selecionar.php",
    type: "POST",
    data: { 'id': x, 'funcao': 2 },
    success: function (res) {
      document.getElementById('editaEquipe').innerHTML = res;
      instance.open();
    }
  });
}
// função para fazer a exclusão pratica de chamados
function funcaoApagarChamados(val) {
  var x = val;
  var r = confirm("Tem certeza que deseja excluir?");
  if (r == true) {
    $.ajax({
      url: "chamados/excluir.php",
      type: "POST",
      data: { 'id': x },
      success: function (res) {
        alert("Excluido com sucesso!");
        location.reload();
      }
    });
  }
}
// funcao para completar as tarefas dos chamados
function funcaoCompletarChamados(val) {
  var x = val;
  var r = confirm("Tarefa completa?");
  if (r == true) {
    $.ajax({
      url: "chamados/atualizar.php",
      type: "POST",
      data: { 'id': x },
      success: function (res) {
        alert(res);
        location.reload();
      }
    });
  }
}
// função para fazer a exclusão pratica de agendamentos
function funcaoApagarAgendamento(val) {
  var x = val;
  var r = confirm("Tem certeza que deseja excluir?");
  if (r == true) {
    $.ajax({
      url: "agendamentos/excluir.php",
      type: "POST",
      data: { 'id': x },
      success: function (res) {
        alert("Excluido com sucesso!");
        location.reload();
      }
    });
  }
}
// funcao para completar as tarefas dos agendamentos
function funcaoCompletarAgendamento(val) {
  var x = val;
  var r = confirm("Tarefa completa?");
  if (r == true) {
    $.ajax({
      url: "agendamentos/atualizar.php",
      type: "POST",
      data: { 'id': x },
      success: function (res) {
        alert(res);
        location.reload();
      }
    });
  }
}
