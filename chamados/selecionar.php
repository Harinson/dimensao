<?php
// funcao para pegar os chamados e mostrar na tabela
function sistema_chamados(){
  include "conexao_db/conexao.php";
  $sql = "SELECT c.data_ctt,cc.nome,c.motivo,c.descricao,c.id,c.status FROM chamados c,clientes cc WHERE cc.id=c.cliente";
  $result=mysqli_query($link, $sql);
  
  if (!$result):
    echo "erro na consulta do banco";
  else:
    while($linha=mysqli_fetch_array($result)){
      $id=$linha['id'];
      if($linha['status']== 0):
        $i = "<i class='material-icons'>check</i>";
      else:
        $i = "<i class='material-icons'>clear</i>";
      endif;
      ?>
      <tr>
            <td><?=$linha['data_ctt']?></td>
            <td><?=$linha['nome']?></td>
            <td><?=$linha['motivo']?></td>
            <td><?=$linha['descricao']?></td>
            <td><?=$i?></td>
            <td>
              <button class="btn-floating btn-medium waves-effect waves-light red" alt="Excluir" onclick="funcaoApagarChamados(<?=$linha['id']?>)"><i class="material-icons">delete</i></button>
              <?php
              if($linha['status']!= 0):
                ?>
                <button class="btn-floating btn-medium waves-effect waves-light green" alt="Excluir" onclick="funcaoCompletarChamados(<?=$id?>)"><i class="material-icons">done_all</i></button>
              <?php
              endif;
              ?>
              
              
            </td>
      </tr>
      <?php
    }
  endif;
}

//gerenciamento do agendamento de cada cliente
function chamados_agd(){
  include "conexao_db/conexao.php";
  $sql = "SELECT c.status,cc.nome,c.id, c.data_ctt FROM chamados c, clientes cc WHERE c.status=1 and cc.id=c.cliente";
  $result=mysqli_query($link, $sql);
  if (!$result):
    echo "erro na consulta do banco";
  else:
      while($linha=mysqli_fetch_array($result)){
        ?>
          <option value="<?=$linha['id']?>"><?=$linha['data_ctt']?> | <?=$linha['nome']?> </option>
        <?php
      }
     
  endif;
}


