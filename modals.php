<!-- Modals do sistema -->

<!-- Modal Adicionar Cliente -->
<div id="addCliente" class="modal">
    <div class="modal-content">
      <h4>Adicionar Cliente</h4>
      <form class="col s12" method="post" action="clientes/cadastrar.php">
      <div class="row">
        <div class="input-field col s12">
          <i class="material-icons prefix">account_circle</i>
          <input  name="nome"  id="nome" type="text" class="validate">
          <label for="nome">Nome Completo</label>
        </div>
        <div class="input-field col s12">
          <i class="material-icons prefix">location_on</i>
          <input name="endereco" id="endereco" type="text" class="validate" data-lenght="100">
          <label for="endereco">Endereco</label>
        </div>
        <div class="input-field col s6">
          <i class="material-icons prefix">phone</i>
          <input name="tell"  id="tell" type="tel" class="validate" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);">
          <label for="tell">Telefone</label>
        </div>
        <div class="input-field col s6">
          <i class="material-icons prefix">email</i>
          <input name="mail"  id="mail" type="email" class="validate">
          <label for="mail">E-mail</label>
        </div>
        <button style="float: right;" name="btn_cad" class="modal-close btn waves-effect waves-light blue" type="submit" name="action">Salvar
          <i class="material-icons right">send</i>
         </button>
      </div>
    </form>
    </div>
</div>

  <!-- Modal Editar Cliente -->
<div id="editCliente" class="modal">
    <div class="modal-content" id="editar_cliente">
      <!-- adição de conteudo via javascript -->
    </div>    
</div>

<!-- Modal Adicionar Equipe -->
<div id="addEquipe" class="modal">
    <div class="modal-content">
      <h4>Adicionar Equipe</h4>
      <form class="col s12" method="post" action="equipe/cadastrar.php">
      <div class="row">
        <div class="input-field col s12">
          <i class="material-icons prefix">account_circle</i>
          <input  name="nome_equipe"  id="nome_equipe" type="text" class="validate">
          <label for="nome_equipe">Nome Completo</label>
        </div>
        <div class="input-field col s6">
          <i class="material-icons prefix">build</i>
          <input name="funcao_equipe"  id="funcao_equipe" type="text" class="validate">
          <label for="funcao_equipe">Função</label>
        </div>
        <div class="input-field col s6">
          <i class="material-icons prefix">phone</i>
          <input name="tell_equipe"  id="tell_equipe" type="tel" class="validate" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);">
          <label for="tell_equipe">Telefone</label>
        </div>
        <button style="float: right;" name="btn_cad" class="modal-close btn waves-effect waves-light blue" type="submit" name="action">Salvar
          <i class="material-icons right">send</i>
         </button>
      </div>
    </form>
    </div>
</div>

  <!-- Modal Editar Cliente -->
<div id="editEquipe" class="modal">
    <div class="modal-content" id="editaEquipe">
      <!-- adição de conteudo via javascript -->
    </div>    
</div>

<!-- Modal Adicionar Chamados -->
<div id="addChamado" class="modal">
    <div class="modal-content">
      <h4>Adicionar Chamado</h4>
      <form class="col s12" method="post" action="chamados/cadastrar.php">
      <div class="row">
        <div class="input-field col s12">
          <select name="cliente_motivo">
            <option value="" disabled selected>Escolha o Cliente</option>
            <?php
            include_once "clientes/selecionar.php";
            clientes_chamados();
            ?>
          </select>
        </div>
        <div class="input-field col s12">
          <i class="material-icons prefix">edit</i>
          <input  name="motivo_chamado"  id="motivo_chamado" type="text" class="validate" data-length="50">
          <label for="motivo_chamado">Motivo da Chamada</label>
        </div>
        <div class="input-field col s12">
          <i class="material-icons prefix">build</i>
          <textarea name="descricao_chamado"  id="descricao_chamado" class="validate materialize-textarea" data-length="50"></textarea>
          <label for="descricao_chamado">Descrição</label>
        </div>
        <div class="input-field col s12">
            <select name="status_chamado">
              <option value="" disabled selected>Escolha o status do chamado...</option>
              <option value="0">Finalizado</option>
              <option value="1">Não Finalizado</option>
            </select>
            
          </div>
        <button style="float: right;" name="btn_cad" class="modal-close btn waves-effect waves-light blue" type="submit" name="action">Salvar
          <i class="material-icons right">send</i>
         </button>
      </div>
    </form>
    </div>
</div>

<!-- Modal Adicionar Chamados -->
<div id="addAgendamento" class="modal">
    <div class="modal-content">
      <h4>Adicionar Agendamento</h4>
      <form class="col s12" method="post" action="agendamentos/cadastrar.php">
      <div class="row">
        <div class="input-field col s12">
          <select name="chamado_agd">
            <option value="" disabled selected>Escolha o Cliente</option>
            <?php
            include_once "chamados/selecionar.php";
            chamados_agd();
            ?>
          </select>
        </div>
        <div class="input-field col s12">
          <select name="equipe_agd">
            <option value="" disabled selected>Escolha o Funcionário</option>
            <?php
            include_once "equipe/selecionar.php";
            equipe_agd();
            ?>
          </select>
        </div>
        <div class="input-field col s12">
          <i class="material-icons prefix">edit</i>
          <input  name="dt_agendado"  id="dt_agendado" type="date" class="validate">
          <label for="dt_agendado">Data de Agendamento</label>
        </div>
        <button style="float: right;" name="btn_cad" class="modal-close btn waves-effect waves-light blue" type="submit" name="action">Agendar
          <i class="material-icons right">send</i>
         </button>
      </div>
    </form>
    </div>
</div>

