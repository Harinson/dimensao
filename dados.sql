-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05-Set-2020 às 18:21
-- Versão do servidor: 10.1.35-MariaDB
-- versão do PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bitbucket`
--

--
-- Extraindo dados da tabela `agendamentos`
--

INSERT INTO `agendamentos` (`id`, `chamado`, `equipe`, `dt_agendamento`, `dt_agendado`, `status`) VALUES
(3, 15, 5, '2020-09-04 23:43:52', '2020-09-15', 1);

--
-- Extraindo dados da tabela `chamados`
--

INSERT INTO `chamados` (`id`, `cliente`, `motivo`, `descricao`, `data_ctt`, `status`) VALUES
(15, 10, 'Problemas na encanaÃ§Ã£o', 'Alguns barulhos ouvidos vindo da encanaÃ§Ã£o', '2020-09-04 20:43:31', 1),
(14, 10, 'Entupiu a pia ', 'adsdadasd', '2020-09-04 19:44:52', 0);

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `nome`, `endereco`, `telefone`, `email`) VALUES
(10, 'Igor Harinson Mendes Monteiro', 'Rua 37', '(98) 99969-9010', 'igor@gmail.com'),
(9, 'Cleber', 'Rua 37', '(98) 99969-9010', 'Gaby@gaby.com'),
(12, 'Jeovani Nascimento', 'Rua 35 Qda 18 Casa 10', '(99) 99999-9999', 'Jeo@gmail.com');

--
-- Extraindo dados da tabela `equipe`
--

INSERT INTO `equipe` (`id`, `nome`, `cargo`, `telefone`) VALUES
(5, 'Roberto Carlos', 'Marceneiro', '(99) 99999-9999'),
(6, 'Pedro Fonseca', 'Gestor de Obras', '(98) 99989-9999');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
